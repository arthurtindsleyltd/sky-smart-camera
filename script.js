const video = document.getElementById('video')

Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
  faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
  faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
  faceapi.nets.faceExpressionNet.loadFromUri('/models')
]).then(startVideo)

// VIDEO SETUP //
document.querySelector('.AR-mode').addEventListener("click", changeVideo);

let videoDevices;

function startVideo() {
  getDevices().then(getVideoDevices).then(function(result) {
    videoDevices = result;
    console.log('Available video devices:', result);
 })
  navigator.getUserMedia(
    { video: {} },
    stream => video.srcObject = stream,
    err => console.error(err)
  )
}

function getDevices() {
  return navigator.mediaDevices.enumerateDevices();
}

function getVideoDevices(allDevices) {

  let devices = [];

  for (i = 0; i < allDevices.length; i++) {
    if (allDevices[i].kind === 'videoinput') {
      devices.push(allDevices[i]);
    }
  }

  return devices;
}

video.addEventListener('play', () => {
  if(ARMode === 1){
    const canvas = faceapi.createCanvasFromMedia(video);
    document.querySelector('.video-container').append(canvas);
    const displaySize = { width: video.width, height: video.height };
    faceapi.matchDimensions(canvas, displaySize);
    setInterval(async () => {
      const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions()
      const resizedDetections = faceapi.resizeResults(detections, displaySize);
      canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
      faceapi.draw.drawDetections(canvas, resizedDetections);
      faceapi.draw.drawFaceLandmarks(canvas, resizedDetections);
      faceapi.draw.drawFaceExpressions(canvas, resizedDetections);

      updatePageCoordinates(detections);

      updatePageEmotions(detections);

    }, 100)
  }
})

let ARMode = 1;

function changeVideo(){

  if(ARMode === 0){ 
    ARMode = 1;
  }else{
    ARMode = 0;

    // Remove the canvas
    let elem = document.querySelector('.video-container').getElementsByTagName("canvas")[0]
    elem.remove();
  }

  const constraints = {
    video: {deviceId: videoDevices[ARMode].deviceId ? {exact: videoDevices[ARMode].deviceId} : undefined}
  };
  
  navigator.mediaDevices.getUserMedia(constraints).then(updateStream).catch(handleError);
}

function updateStream(stream) {
  window.stream = stream;
  video.srcObject = stream;
}

function handleError(error) {
  console.error('Error: ', error);
}

//EXTRACTING CONTENT FROM API AND SENDING TO THE PAGE
// This function only passes the coordinates of the first person detected. 
function updatePageCoordinates(detections){
  let x = 0;
  let y = 0;

  let box = detections[0].alignedRect.box;

  // The documentation is awful and I was getting lower numbers then I suspected. 
  // While looking at the detections output in the concole I noticed a property called imageDims which stays static at 640*480. 
  // I believe that the API is outputing this number instead of the right numbers for my video feed (which makes).
  // So I've multiplied the formular by 2.
  x = (box.x+(box.width/2))*2;
  y = (box.y+(box.height/2))*2;

  document.querySelector('.x').innerHTML = "x: "+Math.floor(x);
  document.querySelector('.y').innerHTML = "y: "+Math.floor(y);
}

// This function only passes the coordinates of the first person detected. 
function updatePageEmotions(detections){
  let primaryEmotion = 'neutral';

  let expressions = detections[0].expressions;

  //Working out the average programmatically instead of just using 0.142857142857143 (1/7 items);
  let average = 1/Object.keys(expressions).length;

  emoji =  document.querySelector('.emoji');

  if(expressions.angry > average){
      emoji.innerHTML = "😡";
  }

  if(expressions.disgusted > average){
    emoji.innerHTML = "🤢";
  }

  if(expressions.fearful > average){
    emoji.innerHTML = "🙀";
  }

  if(expressions.happy > average){
    emoji.innerHTML = "😊";
  }

  if(expressions.neutral > average){
    emoji.innerHTML = "😐";
  }

  if(expressions.sad > average){
    emoji.innerHTML = "😞";
  }

  if(expressions.surprised > average){
    emoji.innerHTML = "😱";
  }

}

// DEVELOPER MODE

function developermode(){

}